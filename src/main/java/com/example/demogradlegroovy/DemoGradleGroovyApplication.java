package com.example.demogradlegroovy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGradleGroovyApplication {

	public static void main(String[] args) {

		SpringApplication.run(DemoGradleGroovyApplication.class, args);
	}

}
